package com.sgu.exampleapplication;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class CustomListView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);

        ArrayList<StudentModel> students = new ArrayList<>();
        students.add(new StudentModel(1149384, "Wilbo", R.drawable.ic_headset_black_24dp));
        students.add(new StudentModel(1120381, "Wereworo", R.drawable.ic_headset_black_24dp));

        ListView lv = findViewById(R.id.custom_list_view_linear_layout);
//        lv.setAdapter(new StudentModelAdapter(students, this));
    }
}
class StudentModelAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}

class StudentModel {

    int studentId;
    String studentName;

    int resourceId;

    public StudentModel(int studentId, String studentName, int resourceId) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.resourceId = resourceId;
    }

    public int getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public int getResourceId() {
        return resourceId;
    }
}