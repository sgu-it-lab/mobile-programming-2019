package com.sgu.exampleapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HumanDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_human_detail);

        Bundle params = getIntent().getExtras();
        String id = params.getString("id","1");
        String name = params.getString("name", "PLACEHOLDER");
        String address = params.getString("address", "ADDRESS");

        TextView idText = findViewById(R.id.human_detail__id_text);
        TextView nameText = findViewById(R.id.human_detail__name_text);
        TextView addressText = findViewById(R.id.human_detail__address_text);

        idText.setText(id);
        nameText.setText(name);
        addressText.setText(address);

    }
}
