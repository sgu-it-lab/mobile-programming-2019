package com.sgu.exampleapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class HumanListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_human_list);

        ArrayList humans = new ArrayList<Human>();
        humans.add(new Human("Wilbert", "001", "Serpong", R.drawable.logo1));
        humans.add(new Human("Wilbert", "002", "Serpong", R.drawable.ic_headset_black_24dp));
        humans.add(new Human("Wilbert", "003", "Serpong", R.drawable.ic_headset_black_24dp));
        humans.add(new Human("Wilbert", "004", "Serpong", R.drawable.ic_headset_black_24dp));
        humans.add(new Human("Wilbert", "005", "Serpong", R.drawable.ic_headset_black_24dp));

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setAdapter(new HumanListAdapter(humans, this));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

    }
}

class HumanListAdapter extends RecyclerView.Adapter<HumanViewHolder> {

    private ArrayList<Human> humans;
    private Context context;

    public HumanListAdapter(ArrayList<Human> humans, Context context) {
        this.humans = humans;
        this.context = context;
    }

    @NonNull
    @Override
    public HumanViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_human_list_view, viewGroup, false);
        return new HumanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HumanViewHolder humanViewHolder, int i) {
        final Human h = humans.get(i);
        humanViewHolder.nameView.setText(h.getName());
        humanViewHolder.addressView.setText(h.getAddress());
        humanViewHolder.idView.setText(h.getId());

        humanViewHolder.imageView.setImageDrawable(
                context.getDrawable(h.getImageResourceId())
        );

        humanViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HumanDetailActivity.class);

                Bundle extras = new Bundle();
                extras.putString("id", h.getId());
                extras.putString("name", h.getName());
                extras.putString("address",h.getAddress());

                intent.putExtras(extras);

                context.startActivity(intent);
            }
        });
        //humanViewHolder.imageView.setOnClickListener();

        //humanViewHolder.imageView.setImageResource(h.getImageResourceId());

    }


    @Override
    public int getItemCount() {
        return humans.size();
    }
}


class HumanViewHolder extends RecyclerView.ViewHolder {

    protected View rootView;
    protected ImageView imageView;
    protected TextView nameView;
    protected TextView addressView;
    protected TextView idView;

    public HumanViewHolder(@NonNull View itemView) {
        super(itemView);

        rootView = itemView;
        imageView = itemView.findViewById(R.id.row_human__image_view);
        nameView = itemView.findViewById(R.id.row_human__name_text);
        idView = itemView.findViewById(R.id.row_human__id_text);
        addressView = itemView.findViewById(R.id.row_human__address_text);
    }
}

class Human {

    private String name;
    private String id;
    private String address;

    private int imageResourceId = R.drawable.ic_headset_black_24dp;

    public Human(String name, String id, String address, int imageResourceId) {
        this.name = name;
        this.id = id;
        this.address = address;
        this.imageResourceId = imageResourceId;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }
}