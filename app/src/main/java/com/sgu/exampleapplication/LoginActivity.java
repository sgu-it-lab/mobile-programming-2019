package com.sgu.exampleapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();

        Button loginButton = findViewById(R.id.login_btn);
        final EditText emailEdit = findViewById(R.id.email_edit);
        EditText passwordEdit = findViewById(R.id.password_edit);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("username", emailEdit.getText().toString());

                intent.putExtras(bundle);

                startActivity(intent);
                finish();
            }
        });

    }
}
