package com.sgu.exampleapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle params = getIntent().getExtras();
        String username = params.getString("username");

        TextView welcomeTextView = findViewById(R.id.welcome_text);
        welcomeTextView.setText("Welcome " + username);






        final TextView tv = findViewById(R.id.text_view_1);
        tv.setText("0");

        final EditText et = findViewById(R.id.edit_text_1);

        Button btn = findViewById(R.id.button_1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                tv.setText(counter+"");
                Log.d("MAIN_ACTIVITY", et.getText().toString());

                Toast.makeText(MainActivity.this,
                        "EditText : " + et.getText().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }


}
