package com.sgu.exampleapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class StringAdapterActivity extends AppCompatActivity {

    public String[] animalStrings = {
            "Dog",
            "Cat",
            "Kitten",
            "Elephant",
            "Ostrich",
            "Chicken",
            "Sheep",
            "Dolphin",
            "Pig",
            "Owl",
            "Emu",
            "Coyote",
            "Wolf",
            "Mouse"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_string_adapter);

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.row_animal_name_2, animalStrings);

        ListView listView = findViewById(R.id.string_adapter_lv);
        listView.setAdapter(adapter);

    }
}
